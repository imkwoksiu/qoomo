﻿using UnityEngine;
using System.Collections;

public class ExitTrigger : MonoBehaviour {
    public triggerAI enter;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Hand")
        {
            enter.triggerTree(false);
        }
    }
}
