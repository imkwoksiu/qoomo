﻿using UnityEngine;
using System.Collections;

public class triggerAI : MonoBehaviour {
    public Transform destination,break00, break01;
    bool shouldMove = false, firstMove = false;
    public GameObject bridge;
    public Qoomo qoomo;
    public HandController hc;
    public GameObject star;
    Vector3 bridgePsition;
    Transform bridgeHand;
	// Use this for initialization
	void Start () {
        bridgePsition = bridge.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (firstMove) {
            qoomo.setTarget(destination);
        }
        if (bridgeHand != null) {
            bridge.transform.position = bridgeHand.position;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hand") {
            triggerTree(true);
            bridgeHand = other.transform;

        }
    }
    void OnTriggerExit(Collider other)
    {

    }
    public void triggerTree(bool trigger) {
        firstMove = trigger;
        shouldMove = trigger;
        bridge.SetActive(trigger);
        hc.EnableMeshRenderer(!trigger);
        if (trigger)
        {
        }
        else {
            bridge.transform.position = bridgePsition;
            bridgeHand = null;
        }
    }
}
