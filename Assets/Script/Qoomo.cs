﻿using UnityEngine;
using System.Collections;

public class Qoomo : MonoBehaviour {
    public Transform target;
    float movingSpeed;
    Animator anim;
    public Transform ui;
    public HandController hc;
    public Transform key;
    Transform tempTarget;
    // Use this for initialization
    void Start () {
        movingSpeed = 500;
        anim = transform.Find("QoomoModel").GetComponent<Animator>();
        tempTarget = null;
    }
	
	// Update is called once per frame
	void Update () {

    }
    void FixedUpdate()
    {
        goForKey();
        MoveToward();
    }
    void MoveToward() {
        if (target != null)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.Normalize(target.position - transform.position) * movingSpeed);
            anim.SetBool("walking", true);
        }
        else {
            anim.SetBool("walking", false);
        }

    }
    public void setTarget(Transform target) {
        this.target = target;
    }
    void OnCollisionEnter(Collision other)
    {
        switch (other.transform.tag)
        {
            case "Danger":
                OnDie();
                break;
            case "Key":
                other.gameObject.SetActive(false);
                key = null;
                target = tempTarget;
                tempTarget = null;
                break;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        switch (other.transform.tag)
        {
            case "Danger":
            OnDie();
                break;
            case "Des":
                OnWin();
                break;
        }
    }
    void OnDie() {
        Util.Log("Die");
        target = null;
        ui.GetComponent<UI>().fail();
    }
    void OnWin() {
        Util.Log("Win");
        target = null;
        ui.GetComponent<UI>().win();
    }
    bool goForKey()
    {
        if (key == null)
            return false;
        if ((key.position - transform.position).magnitude < 11)
        {
            if(tempTarget == null)
                tempTarget = target;
            target = key;
            return true;
        }
        return false;
    }
}
