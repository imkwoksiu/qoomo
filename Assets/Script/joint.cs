﻿using UnityEngine;
using System.Collections;

public class joint : MonoBehaviour
{
    FixedJoint spJoint;
    int fingerCount;
    MagneticPinch hand;
    // Use this for initialization
    void Start()
    {
        fingerCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (hand != null && !hand.isPinching)
        {
            if (spJoint != null && spJoint.connectedBody == null)
            {
                Destroy(spJoint);
                hand = null;
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Finger" && hand.isPinching && spJoint == null)
        {
            hand = other.transform.parent.parent.GetComponent<MagneticPinch>();
            spJoint = gameObject.AddComponent<FixedJoint>();
            spJoint.connectedBody = other.rigidbody;
        }
    }
}
