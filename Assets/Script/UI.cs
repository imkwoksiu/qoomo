﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {
    public GameObject selectStageUI, startMenuUI;
    public GameObject pauseButton, playButton;
    public HandController hc;
    bool pause,isFail;
    private float time;
    public Text timerLabel;
    public GameObject star1, star2, star3;
    public GameObject ready;
    public GameObject rock;
    public Qoomo qoomo;
    public Transform des;
    // Use this for initialization
    void Start () {
        pause = true;
        isFail = false;
        if(hc != null)
        hc.useCameraControl = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (!pause)
            time += Time.deltaTime;

        var minutes = time / 60; //Divide the guiTime by sixty to get the minutes.
        var seconds = time % 60;//Use the euclidean division for the seconds.

        //update the label value
        if(timerLabel != null)
        timerLabel.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    public void changeSceneByNum(int num) {
        Application.LoadLevel(num);
        Time.timeScale = 1;
    }
    public void reloadScene() {
        changeSceneByNum(Application.loadedLevel);
    }
    public void nextScene()
    {
        changeSceneByNum(Application.loadedLevel + 1);
    }
    public void triggerSelectStage() {
            selectStageUI.SetActive(!selectStageUI.activeSelf);
        startMenuUI.SetActive(!startMenuUI.activeSelf);
    }
    public void triggerPause() {
        if (isFail)
            return;
        if (!pause)
        {
            transform.Find("tryAgain_UI").gameObject.SetActive(true);
            Debug.Log("pause");
            Time.timeScale = 0;
            pause = true;
            pauseButton.SetActive(false);
            playButton.SetActive(true);
            hc.pause = true;
        }
        else {
            transform.Find("tryAgain_UI").gameObject.SetActive(false);
            Debug.Log("play");
            Time.timeScale = 1;
            pause = false;
            pauseButton.SetActive(true);
            playButton.SetActive(false);
            hc.pause = false;
        }
    }
    public void fail()
    {
        triggerPause();
        isFail = true;
        pauseButton.SetActive(false);
        playButton.SetActive(false);
    }
    public void win() {
        pause = true;
        pauseButton.SetActive(false);
        playButton.SetActive(false);
        transform.Find("star").gameObject.SetActive(true);
        if (time > 15)
        {
            star3.SetActive(false);
        }
        else if (time > 30)
        {
            star2.SetActive(false);
        }
        else if (time > 45) {
            star1.SetActive(false);
        }
        GetComponent<AudioSource>().Play();
    }
    public void triggerTu() {
        gameObject.SetActive(false);
        hc.useCameraControl = false;
    }
    public void readyGo() {
        pause = false;
        ready.SetActive(false);
        hc.useCameraControl = false;
        if (qoomo != null) {
            qoomo.target = des;
        }

        if (rock != null) {
            Rigidbody[] rocks = rock.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody rg in rocks)
            {
                rg.useGravity = true;
            }
        }
    }
}
