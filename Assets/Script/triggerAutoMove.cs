﻿using UnityEngine;
using System.Collections;

public class triggerAutoMove : MonoBehaviour {
    public Transform destination;
    public float AISpeed;
    public Transform ai;
    public GameObject aiModel;
    bool shouldMove = false;
    Animator aiAnimator;
    // Use this for initialization
    void Start () {
        aiAnimator = aiModel.GetComponent<Animator>();
        shouldMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (shouldMove)
        {
            float step = AISpeed * Time.deltaTime;
            aiAnimator.SetBool("walking", true);
            Vector3 des = new Vector3(destination.position.x, ai.position.y, ai.position.z);
            //ai.position = Vector3.MoveTowards(ai.position, des, step);
            ai.GetComponent<Rigidbody>().velocity = (Vector3.Normalize(des - ai.position) * AISpeed);
        }
        else {
            aiAnimator.SetBool("walking", false);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "rock")
        {
            shouldMove = true;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "rock")
        {
            shouldMove = false;
        }
    }
}
